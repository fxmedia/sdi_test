// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BlueprintWebSocketWrapper.h"
#include "ReplyLiveStructs.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "VRBANISM_0043/Level/CarouselManager.h"
#include "VRBANISM_0043/Level/InviteeManager.h"
#include "VRBANISM_0043/Level/PollManager.h"
#include "VRBANISM_0043/Level/SceneManager.h"
#include "WebsocketManager.generated.h"


UCLASS()
class VRBANISM_0043_API AWebsocketManager : public AActor
{
	GENERATED_BODY()

public:  // Public variables
	// Websocket protocol
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Websocket)
	FString WebsocketProtocol = "";
	
	// Reply live server
	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = Websocket)
	FString ReplyLiveServer = "staging-ws";
	
	// Event token
	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = Websocket)
	FString EventToken = "iZgxKClINVxfFXVJMLXK5qwgqFEzaEzX";
	
	// Location token, find in reply.live/nova/resources/locations/<location_id> as Token 
	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = Websocket)
	FString LocationToken = "a201d764-2078-48f1-8508-b3e1496ad67c";

	// Should we reconnect. Advised to keep as true, since the websocket will break the connection after 30 seconds of inactivity
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Websocket)
	bool ShouldReconnect = true;

	// Websocket URL
	UPROPERTY(BlueprintReadOnly, Category = Websocket)
	FString WebsocketUrl;

	// API url
	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = API)
	FString APIUrl = "http://staging.reply.live";

	// API path
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = API)
	FString APIPath = "/reply-live-api/";

	// Location token, find in reply.live/nova/resources/locations/<location_id> as Token 
	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = API)
	FString APILocationToken = "a201d764-2078-48f1-8508-b3e1496ad67c";
	
	// InviteeManager, used to spawn and manage invitees
	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = Managers)
	class AInviteeManager* InviteeManager;

	// Scenemanager, used to manage the NDI receivers visibility
	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = Managers)
	class ASceneManager* SceneManager;

	// Carouselmanager, used to  manage the carousel objects visibility
	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = Managers)
	class ACarouselManager* CarouselManager;

	// Pollmanager, used to spawn and manage the poll
	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = Managers)
	class APollManager* PollManager;

	// WebSocket, marking it as UPROPERTY prenvents it from being garbage collected as actions are latent.
	UPROPERTY()
	UBlueprintWebSocket* WebSocket;

	FHttpModule * Http;

	/* Handle to manage the timer */
	FTimerHandle PollTimerHandle;
	FTimerDelegate PollTimerDel;

public:  // Public functions
	// Sets default values for this actor's properties
	AWebsocketManager();

	/*
	Blueprint implementable events, these are used to expose websocket events that are not yet implemented internally
	*/

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void CameraUpdate(FReplyLiveLocationMessage message);
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void SimulationStart(FReplyLiveLocationMessage message);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void PostUpdated(FReplyLivePostMessage message);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void PostFavoriteAdd(FReplyLivePostMessage message);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void PostFavoriteRemove(FReplyLivePostMessage message);

	// API requests
	UFUNCTION(BlueprintCallable)
	void GetInvitees();

	UFUNCTION(BlueprintCallable)
	void GetPollAnswers(int32 ActivePollId);

private:
	// Create and connect our socket to the WebSocket server.
	void InitializeAndConnectSocket();

	// Websocket callbacks
	UFUNCTION() void OnConnected();
	UFUNCTION() void OnConnectionError(const FString & Error);
	UFUNCTION() void OnClosed(int64 StatusCode, const FString & Reason, bool bWasClean);
	UFUNCTION() void OnMessage(const FString & Message);
	UFUNCTION() void OnRawMessage(const TArray<uint8> & Data, int32 BytesRemaining);
	UFUNCTION() void OnMessageSent(const FString & Message);

	// API callbacks
	void ReceiveInvitees(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void ReceivePollAnswers(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	// Parsers
	UFUNCTION() FReplyLiveMessage ParseMessage(const FString & Message);
	UFUNCTION() FReplyLivePollMessage ParsePollMessage(const FReplyLiveMessage & ReplyLiveMessage);
	UFUNCTION() FReplyLiveLocationMessage ParseLocationMessage(const FReplyLiveMessage & ReplyLiveMessage);
	UFUNCTION() FReplyLiveObjectMessage ParseObjectMessage(const FReplyLiveMessage & ReplyLiveMessage);
	UFUNCTION() FReplyLivePostMessage ParsePostMessage(const FReplyLiveMessage & ReplyLiveMessage);
	UFUNCTION() FReplyLiveChatroomMessage ParseChatroomMessage(const FReplyLiveMessage & ReplyLiveMessage);
	UFUNCTION() FReplyLiveInviteeMessage ParseInviteeMessage(const FReplyLiveMessage & ReplyLiveMessage);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
