// Fill out your copyright notice in the Description page of Project Settings.


#include "JsonObjectConverter.h"
#include "WebsocketManager.h"

// Sets default values
AWebsocketManager::AWebsocketManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// HTTP request
	Http = &FHttpModule::Get();
}

// Called when the game starts or when spawned
void AWebsocketManager::BeginPlay()
{
	Super::BeginPlay();
	
	// Make WebsocketUrl from user edited values
	WebsocketUrl = "wss://" + ReplyLiveServer + ".reply.live/app/" + EventToken;

	InitializeAndConnectSocket();

	// Get Invitees that are currently in the event
	GetInvitees();
}

// Called every frame
void AWebsocketManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWebsocketManager::InitializeAndConnectSocket()
{
	// Create new BlueprintWebsocket object
	WebSocket = UBlueprintWebSocket::CreateWebSocket();

	// Bind the events so our functions are called when the events are triggered
	WebSocket->OnConnectedEvent.AddDynamic(this, &AWebsocketManager::OnConnected);
	WebSocket->OnConnectionErrorEvent.AddDynamic(this, &AWebsocketManager::OnConnectionError);
	WebSocket->OnCloseEvent.AddDynamic(this, &AWebsocketManager::OnClosed);
	WebSocket->OnMessageEvent.AddDynamic(this, &AWebsocketManager::OnMessage);
	WebSocket->OnRawMessageEvent.AddDynamic(this, &AWebsocketManager::OnRawMessage);
	WebSocket->OnMessageSentEvent.AddDynamic(this, &AWebsocketManager::OnMessageSent);

	// Connect to the websocket
	UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::InitializeAndConnectSocket Connecting to: %s"), *WebsocketUrl);
	WebSocket->Connect(WebsocketUrl, WebsocketProtocol);
}

void AWebsocketManager::OnConnected()
{
	UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnConnected We are connected!"));

	ShouldReconnect = true;
}

void AWebsocketManager::OnConnectionError(const FString & Error)
{
	UE_LOG(LogTemp, Error, TEXT("AWebsocketManager::OnConnectionError Failed to connect: %s."), *Error);
}

void AWebsocketManager::OnClosed(int64 StatusCode, const FString & Reason, bool bWasClean)
{
	UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::OnClosed Connection closed: %d:%s. Clean: %d"), StatusCode, *Reason, bWasClean);

	if (ShouldReconnect) {
		UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnClosed Reconnecting to: %s"), *WebsocketUrl);
		WebSocket->Connect(WebsocketUrl, WebsocketProtocol);
	}

	ShouldReconnect = false;
}

void AWebsocketManager::OnMessage(const FString & Message)
{
	// Parse the channel and event to see how we need to process the message. This does not parse the data
	FReplyLiveMessage ParsedMessage = ParseMessage(Message);

	/* 
	Connection messages
	*/
	if (ParsedMessage.event == "pusher:connection_established") {
		UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnMessage Received Connection Established event"));
		
		// Create request to subscribe to events from the connection
		FString ConnectionEstablishedMessage = "{\"event\":\"pusher:subscribe\",\"data\":{\"auth\":\"\",\"channel\":\"replylive.location." + LocationToken + "\"}}";

		// Send Subscribe event to the websocket
		WebSocket->SendMessage(ConnectionEstablishedMessage);
	}
	else if (ParsedMessage.event == "pusher_internal:subscription_succeeded") {
		UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::OnMessage pusher_internal:subscription_succeeded event not implemented"));
	}
	
	/* 
	Location messages 
	*/
	else if (ParsedMessage.event == "CameraUpdate") {
		UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnMessage Received CameraUpdate event"));

		// Parse location data
		FReplyLiveLocationMessage ParsedLocationMessage = ParseLocationMessage(ParsedMessage);

		// Trigger corresponding event
		CameraUpdate(ParsedLocationMessage);
	}
	else if (ParsedMessage.event == "SceneUpdate") {
		UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnMessage Received SceneUpdate event"));

		// Parse location data
		FReplyLiveLocationMessage ParsedLocationMessage = ParseLocationMessage(ParsedMessage);

		// Trigger corresponding event
		if (SceneManager) {
			SceneManager->UpdateScene(ParsedLocationMessage.data);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::OnMessage No SceneManager selected"));
		}
	}	
	else if (ParsedMessage.event == "SimulationStart") {
		UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnMessage Received SimulationStart event"));

		// Parse location data
		FReplyLiveLocationMessage ParsedLocationMessage = ParseLocationMessage(ParsedMessage);

		// Trigger corresponding event
		SimulationStart(ParsedLocationMessage);
	}

	/* 
	Carousel Object messages
	*/
	else if (ParsedMessage.event == "CarouselObjectShow") {
		UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnMessage Received CarouselObjectShow event"));

		// Parse object data
		FReplyLiveObjectMessage ParsedObjectMessage = ParseObjectMessage(ParsedMessage);

		// Trigger corresponding event
		if (CarouselManager) {
			CarouselManager->UpdateObject(ParsedObjectMessage.data);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::OnMessage No CarouselManager selected"));
		}

	}

	/* 
	Poll messages
	*/
	else if (ParsedMessage.event == "PollStart") {
		UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnMessage Received PollStart event"));

		// Parse poll data
		FReplyLivePollMessage ParsedPollMessage = ParsePollMessage(ParsedMessage);

		// Trigger corresponding event
		if (PollManager) {
			PollManager->StartPoll(ParsedPollMessage.data);

			// Params to pass into function once it ticks
			int32 PollID = ParsedPollMessage.data.id;

			//Binding the function with specific variables
			PollTimerDel.BindUFunction(this, FName("GetPollAnswers"), PollID);
			GetWorld()->GetTimerManager().SetTimer(PollTimerHandle, PollTimerDel, PollManager->PollRate, true);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::OnMessage No PollManager selected"));
		}
	}
	else if (ParsedMessage.event == "PollPause") {
		UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnMessage Received PollPause event"));

		// Parse poll data
		FReplyLivePollMessage ParsedPollMessage = ParsePollMessage(ParsedMessage);

		// Trigger corresponding event
		if (PollManager) {
			PollManager->PauzePoll(ParsedPollMessage.data);

			// Clear timer
			GetWorld()->GetTimerManager().ClearTimer(PollTimerHandle);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::OnMessage No PollManager selected"));
		}
	}
	else if (ParsedMessage.event == "PollStop") {
		UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnMessage Received PollStop event"));

		// Parse poll data
		FReplyLivePollMessage ParsedPollMessage = ParsePollMessage(ParsedMessage);

		// Trigger corresponding event
		if (PollManager) {
			PollManager->StopPoll(ParsedPollMessage.data);

			// Clear timer
			GetWorld()->GetTimerManager().ClearTimer(PollTimerHandle);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::OnMessage No PollManager selected"));
		}
	}
	else if (ParsedMessage.event == "PollClear") {
		UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnMessage Received PollClear event"));

		// Parse poll data
		FReplyLivePollMessage ParsedPollMessage = ParsePollMessage(ParsedMessage);

		// Trigger corresponding event
		if (PollManager) {
			PollManager->ClearPoll(ParsedPollMessage.data);

			// Clear timer
			GetWorld()->GetTimerManager().ClearTimer(PollTimerHandle);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::OnMessage No PollManager selected"));
		}
	}

	/* 
	Chatroom messages
	*/
	else if (ParsedMessage.event == "PostsHide") {
		UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnMessage Received PostsHide event"));
		
		// Parse chatroom data
		FReplyLiveChatroomMessage ParsedChatroomMessage = ParseChatroomMessage(ParsedMessage);

		// Trigger corresponding event
		if (InviteeManager) {
			InviteeManager->HideAllPosts();
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::OnMessage No InviteeManager selected"));
		}
	}
	else if (ParsedMessage.event == "PostsShow") {
		UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnMessage Received PostsShow event"));

		// Parse chatroom data
		FReplyLiveChatroomMessage ParsedChatroomMessage = ParseChatroomMessage(ParsedMessage);

		// Trigger corresponding event
		if (InviteeManager) {
			InviteeManager->ShowAllPosts();
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::OnMessage No InviteeManager selected"));
		}
	}

	/* 
	Posts messages
	*/
	else if (ParsedMessage.event == "PostCreated") {
		UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnMessage Received PostCreated event"));

		// Parse post data
		FReplyLivePostMessage ParsedPostMessage = ParsePostMessage(ParsedMessage);

		// Trigger corresponding event
		if (InviteeManager) {
			InviteeManager->Posted(ParsedPostMessage.data);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::OnMessage No InviteeManager selected"));
		}
	}
	else if (ParsedMessage.event == "UserEmoji") {
		UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnMessage Received UserEmoji event"));

		// Parse post data
		FReplyLivePostMessage ParsedPostMessage = ParsePostMessage(ParsedMessage);

		// Trigger corresponding event
		if (InviteeManager) {
			InviteeManager->PostedEmoji(ParsedPostMessage.data);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::OnMessage No InviteeManager selected"));
		}
	}
	else if (ParsedMessage.event == "PostUpdated") {
		UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnMessage Received PostUpdated event"));

		// Parse post data
		FReplyLivePostMessage ParsedPostMessage = ParsePostMessage(ParsedMessage);

		// Trigger corresponding event
		PostUpdated(ParsedPostMessage);
	}
	else if (ParsedMessage.event == "PostHighlight") {
		UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnMessage Received PostHighlight event"));

		// Parse post data
		FReplyLivePostMessage ParsedPostMessage = ParsePostMessage(ParsedMessage);

		// Trigger corresponding event
		if (InviteeManager) {
			InviteeManager->HighlightPost(ParsedPostMessage.data);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::OnMessage No InviteeManager selected"));
		}
	}	
	else if (ParsedMessage.event == "ClearHighlight") {
		UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnMessage Received ClearHighlight event"));

		// No need to parse data, since we just get an empty list.
		// ParsedMessage.data == '[]'

		// Trigger corresponding event
		if (InviteeManager) {
			InviteeManager->UnHighlightAllPosts();
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::OnMessage No InviteeManager selected"));
		}
	}	
	else if (ParsedMessage.event == "PostFavoriteAdd") {
		UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnMessage Received PostFavoriteAdd event"));

		// Parse post data
		FReplyLivePostMessage ParsedPostMessage = ParsePostMessage(ParsedMessage);

		// Trigger corresponding event
		PostFavoriteAdd(ParsedPostMessage);
	}
	else if (ParsedMessage.event == "PostFavoriteRemove") {
		UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnMessage Received PostFavoriteRemove event"));

		// Parse post data
		FReplyLivePostMessage ParsedPostMessage = ParsePostMessage(ParsedMessage);

		// Trigger corresponding event
		PostFavoriteRemove(ParsedPostMessage);
	}

	/* 
	Avatar messaes
	*/
	else if (ParsedMessage.event == "InviteeJoined") {
		UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnMessage Received InviteeJoined event"));

		// Parse invitee data
		FReplyLiveInviteeMessage ParsedInviteeMessage = ParseInviteeMessage(ParsedMessage);

		// Trigger corresponding event
		if (InviteeManager) {
			InviteeManager->SpawnInvitee(ParsedInviteeMessage.data);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::OnMessage No InviteeManager selected"));
		}
	}
	else if (ParsedMessage.event == "InviteeUpdated") {
		UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnMessage Received InviteeUpdated event"));

		// Parse invitee data
		FReplyLiveInviteeMessage ParsedInviteeMessage = ParseInviteeMessage(ParsedMessage);

		// Trigger corresponding event
		if (InviteeManager) {
			InviteeManager->UpdateInvitee(ParsedInviteeMessage.data);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::OnMessage No InviteeManager selected"));
		}
	}

	/*
	Unhandled event
	*/
	else {
		UE_LOG(LogTemp, Error, TEXT("AWebsocketManager::OnMessage Received event: %s. not implemented"), *ParsedMessage.event);
	}
}

void AWebsocketManager::OnRawMessage(const TArray<uint8> & Data, int32 BytesRemaining)
{
	// We received a binary message.
	UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnRawMessage New binary message: %d bytes and %d bytes remaining."), Data.Num(), BytesRemaining);
}

void AWebsocketManager::OnMessageSent(const FString & Message)
{
	// We just sent a message
	UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::OnMessageSent Sent message to the server: %s"), *Message);
}

FReplyLiveMessage AWebsocketManager::ParseMessage(const FString & Message)
{
	FReplyLiveMessage EventMessage;
	bool success = FJsonObjectConverter::JsonObjectStringToUStruct(Message, &EventMessage, 0, 0);

	if (success) {
		return EventMessage;
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("AWebsocketManager::ParseMessage Unable to parse message: %s"), *Message);
		return EventMessage;
	}
}

FReplyLivePollMessage AWebsocketManager::ParsePollMessage(const FReplyLiveMessage & ReplyLiveMessage)
{	
	FReplyLivePollMessage PollMessage;
	PollMessage.channel = ReplyLiveMessage.channel;
	PollMessage.event = ReplyLiveMessage.event;

	FReplyLivePollData PollDataMessage;
	bool success = FJsonObjectConverter::JsonObjectStringToUStruct(ReplyLiveMessage.data, &PollDataMessage, 0, 0);

	PollMessage.data = PollDataMessage;

	if (success) {
		return PollMessage;
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("AWebsocketManager::ParsePollMessage Unable to parse poll data: %s"), *ReplyLiveMessage.data);
		return PollMessage;
	}
}

FReplyLiveLocationMessage AWebsocketManager::ParseLocationMessage(const FReplyLiveMessage & ReplyLiveMessage) 
{
	FReplyLiveLocationMessage LocationMessage;
	LocationMessage.channel = ReplyLiveMessage.channel;
	LocationMessage.event = ReplyLiveMessage.event;

	FReplyLiveLocationData LocationDataMessage;
	bool success = FJsonObjectConverter::JsonObjectStringToUStruct(ReplyLiveMessage.data, &LocationDataMessage, 0, 0);

	LocationMessage.data = LocationDataMessage;

	if (success) {
		return LocationMessage;
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("AWebsocketManager::ParseLocationMessage Unable to parse location data: %s"), *ReplyLiveMessage.data);
		return LocationMessage;
	}
}

FReplyLiveObjectMessage AWebsocketManager::ParseObjectMessage(const FReplyLiveMessage & ReplyLiveMessage) 
{
	FReplyLiveObjectMessage ObjectMessage;
	ObjectMessage.channel = ReplyLiveMessage.channel;
	ObjectMessage.event = ReplyLiveMessage.event;

	FReplyLiveObjectData ObjectDataMessage;
	bool success = FJsonObjectConverter::JsonObjectStringToUStruct(ReplyLiveMessage.data, &ObjectDataMessage, 0, 0);

	ObjectMessage.data = ObjectDataMessage;

	if (success) {
		return ObjectMessage;
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("AWebsocketManager::ParseObjectMessage Unable to parse message: %s"), *ReplyLiveMessage.data);
		return ObjectMessage;
	}
}

FReplyLivePostMessage AWebsocketManager::ParsePostMessage(const FReplyLiveMessage & ReplyLiveMessage)
{
	FReplyLivePostMessage PostMessage;
	PostMessage.channel = ReplyLiveMessage.channel;
	PostMessage.event = ReplyLiveMessage.event;

	FReplyLivePostData PostDataMessage;
	bool success = FJsonObjectConverter::JsonObjectStringToUStruct(ReplyLiveMessage.data, &PostDataMessage, 0, 0);

	PostMessage.data = PostDataMessage;

	if (success) {
		return PostMessage;
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("AWebsocketManager::ParsePostMessage Unable to parse post data: %s"), *ReplyLiveMessage.data);
		return PostMessage;
	}
}

FReplyLiveChatroomMessage AWebsocketManager::ParseChatroomMessage(const FReplyLiveMessage & ReplyLiveMessage)
{
	FReplyLiveChatroomMessage ChatroomMessage;
	ChatroomMessage.channel = ReplyLiveMessage.channel;
	ChatroomMessage.event = ReplyLiveMessage.event;

	FReplyLiveChatroomData ChatroomDataMessage;
	bool success = FJsonObjectConverter::JsonObjectStringToUStruct(ReplyLiveMessage.data, &ChatroomDataMessage, 0, 0);

	ChatroomMessage.data = ChatroomDataMessage;

	if (success) {
		return ChatroomMessage;
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("AWebsocketManager::ParseChatroomMessage Unable to parse chatroom data: %s"), *ReplyLiveMessage.data);
		return ChatroomMessage;
	}
}

FReplyLiveInviteeMessage AWebsocketManager::ParseInviteeMessage(const FReplyLiveMessage & ReplyLiveMessage)
{
	FReplyLiveInviteeMessage InviteeMessage;
	InviteeMessage.channel = ReplyLiveMessage.channel;
	InviteeMessage.event = ReplyLiveMessage.event;

	FReplyLiveInvitee InviteeDataMessage;
	bool success = FJsonObjectConverter::JsonObjectStringToUStruct(ReplyLiveMessage.data, &InviteeDataMessage, 0, 0);

	InviteeMessage.data = InviteeDataMessage;

	if (success) {
		return InviteeMessage;
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("AWebsocketManager::ParseInviteeMessage Unable to parse invitee data: %s"), *ReplyLiveMessage.data);
		return InviteeMessage;
	}
}

void AWebsocketManager::GetInvitees()
{
	TSharedRef<IHttpRequest, ESPMode::ThreadSafe> Request = Http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &AWebsocketManager::ReceiveInvitees);

	FString RequestURL = APIUrl + APIPath + APILocationToken + "/virtual-studio/joined";
	Request->SetURL(RequestURL);

	Request->SetVerb("GET");

	Request->SetHeader(TEXT("User-Agent"), TEXT("X-UnrealEngine-Agent"));
	Request->SetHeader(TEXT("Content-Type"), TEXT("application/json"));

	UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::GetInvitees sending GET request to: %s"), *RequestURL);

	Request->ProcessRequest();
}

void AWebsocketManager::GetPollAnswers(int32 ActivePollId)
{
	TSharedRef<IHttpRequest, ESPMode::ThreadSafe> Request = Http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &AWebsocketManager::ReceivePollAnswers);

	FString RequestURL = APIUrl + APIPath + APILocationToken + "/poll/" + FString::FromInt(ActivePollId) + "/answers";

	Request->SetURL(RequestURL);
	Request->SetVerb("GET");

	Request->SetHeader(TEXT("User-Agent"), TEXT("X-UnrealEngine-Agent"));
	Request->SetHeader(TEXT("Content-Type"), TEXT("application/json"));

	UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::GetPollAnswers sending GET request to %s"), *RequestURL);

	Request->ProcessRequest();
}

void AWebsocketManager::ReceiveInvitees(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	FReplyLiveAPIInviteesJoined APIInvitees;
	bool success = FJsonObjectConverter::JsonObjectStringToUStruct(Response->GetContentAsString(), &APIInvitees, 0, 0);

	if (success) {
		if (InviteeManager) {
			for (FReplyLiveInvitee Invitee : APIInvitees.invitees) {
				InviteeManager->SpawnInvitee(Invitee);
			}
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::ReceiveInvitees No InviteeManager found"));
		}
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("AWebsocketManager::ReceiveInvitees Unable to parse invitee joined api data: %s"), *Response->GetContentAsString());
	}
}

void AWebsocketManager::ReceivePollAnswers(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	// Add "{\"poll_answers\":" in front and "}" behind to make parsing easy
	FString PollAnswersString = "{\"poll_answers\":" + Response->GetContentAsString() + "}";
	
	FReplyLiveAPIPollAnswers APIPollAnswers;
	bool success = FJsonObjectConverter::JsonObjectStringToUStruct(PollAnswersString, &APIPollAnswers, 0, 0);

	if (success) {
		if (PollManager) {
			PollManager->GetPollAnswers(APIPollAnswers);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::ReceivePollAnswers No PollManager found"));
		}
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("AWebsocketManager::ReceivePollAnswers Unable to parse poll answers api data: %s"), *Response->GetContentAsString());
	}
}

void AWebsocketManager::CameraUpdate_Implementation(FReplyLiveLocationMessage message)
{
	UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::CameraUpdate_Implementation Executing in C++"));
}

void AWebsocketManager::SimulationStart_Implementation(FReplyLiveLocationMessage message)
{
	UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::SimulationStart_Implementation Executing in C++"));
}

void AWebsocketManager::PostUpdated_Implementation(FReplyLivePostMessage message)
{
	UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::PostUpdated_Implementation Executing in C++"));
}

void AWebsocketManager::PostFavoriteAdd_Implementation(FReplyLivePostMessage message)
{
	UE_LOG(LogTemp, Warning, TEXT("AWebsocketManager::PostFavoriteAdd_Implementation Executing in C++"));
}

void AWebsocketManager::PostFavoriteRemove_Implementation(FReplyLivePostMessage message)
{
	UE_LOG(LogTemp, Log, TEXT("AWebsocketManager::CameraUpdate_Implementation Executing in C++"));
}