// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DownloadManager.h"
#include "GameFramework/Actor.h"
#include "Invitee.h"
#include "SeatsManager.h"
#include "VRBANISM_0043/Websocket/ReplyLiveStructs.h"
#include "InviteeManager.generated.h"

UCLASS()
class VRBANISM_0043_API AInviteeManager : public AActor
{
	GENERATED_BODY()

public:  // Public variables
	// Select which Invitee should be spawned during the event
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Invitee)
	TSubclassOf<AInvitee> InviteeBlueprint;

	// Map all current Invitees in the level
	UPROPERTY(BlueprintReadWrite, Category = Invitees)
	TMap<int32, AInvitee*> Invitees;

	// Flag to track if incomming posts should be hidden
	UPROPERTY(BlueprintReadWrite, Category = Invitees)
	bool isPostsHidden = true;

	// Flag to track if we have a highlight
	UPROPERTY(BlueprintReadWrite, Category = Invitees)
	bool hasHighlightPost;

	// SeatManager, used to find a spawn location for an Invitee
	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = Managers)
	ASeatsManager* SeatsManager;

	// DownloadManager, used to download the Invitee avatars without crashing
	//UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = Managers)
	//ADownloadManager* DownloadManager;

	// HighlightCamera, used to focus on the highlighted post
	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = Highlight)
	AActor* HighlightCamera;

	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = Highlight)
	int32 HighlightCameraDistance = 10;

	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = Highlight)
	float HighlightCameraHeight = 400.0;

	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = Highlight)
	float HighlightCameraFocuspointHeight = 150.0;


public:  // Public functions
	// Sets default values for this actor's properties
	AInviteeManager();

	UFUNCTION(BlueprintCallable) 
	void SpawnInvitee(FReplyLiveInvitee invitee);

	UFUNCTION(BlueprintCallable)
	void AddInvitee(AInvitee* invitee);

	UFUNCTION(BlueprintCallable)
	void RemoveInvitee(AInvitee* invitee);	
	
	UFUNCTION(BlueprintCallable)
	void hasSpawnedInvitee(int32 InviteeId, bool& hasSpawnedInvitee, AInvitee*& Invitee);

	UFUNCTION(BlueprintCallable)
	TArray<AInvitee*> GetInvitees();

	UFUNCTION()	void UpdateInvitee(FReplyLiveInvitee invitee);
	UFUNCTION()	void Posted(FReplyLivePostData PostData);
	UFUNCTION()	void PostedEmoji(FReplyLivePostData PostEmojiData);
	UFUNCTION()	void HideAllPosts();
	UFUNCTION()	void ShowAllPosts();
	UFUNCTION()	void HighlightPost(FReplyLivePostData PostData);
	UFUNCTION()	void UnHighlightPost(FReplyLivePostData PostData);
	UFUNCTION()	void UnHighlightAllPosts();

	// Invitee which is currently highlighted
	AInvitee* HighlightedInvitee;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
