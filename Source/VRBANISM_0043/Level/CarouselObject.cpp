// Fill out your copyright notice in the Description page of Project Settings.


#include "CarouselObject.h"

// Sets default values
ACarouselObject::ACarouselObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACarouselObject::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACarouselObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACarouselObject::ShowCarouselObject_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("ACarouselObject::ShowCarouselObject Executing in C++"));
	this->SetActorHiddenInGame(false);
}

void ACarouselObject::HideCarouselObject_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("ACarouselObject::HideCarouselObject Executing in C++"));
	this->SetActorHiddenInGame(true);
}