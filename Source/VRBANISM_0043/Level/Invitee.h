// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/AsyncTaskDownloadImage.h"
#include "Blueprint/UserWidget.h"
#include "Components/SpotLightComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/WidgetComponent.h"
#include "CoreMinimal.h"
#include "Engine/Texture2D.h"
#include "GameFramework/Actor.h"
#include "InviteeEmojiWidget.h"
#include "InviteeTextWidget.h"
#include "Materials/MaterialInterface.h"
#include "VRBANISM_0043/Websocket/ReplyLiveStructs.h"
#include "Invitee.generated.h"

UCLASS()
class VRBANISM_0043_API AInvitee : public AActor
{
	GENERATED_BODY()

public:  // Public variables
	UPROPERTY()
	UInviteeTextWidget* TextWidgetFront;
	
	UPROPERTY()
	UInviteeTextWidget* TextWidgetBack;

	// Dynamic material to project avatar onto
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Invitee)
	UMaterialInterface* DynamicMaterialDefault;

	// Dynamic material to project avatar onto
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Invitee)
	float HighlightOffset;

	// Fallback avatars when user has not uploaded an avatar image
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Invitee)
	TArray<UTexture2D*> FallbackAvatars;

	// Emoji Widget that is used to show Emoji's
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Widget)
	TSubclassOf<UInviteeEmojiWidget> EmojiWidget;

	// Text Widget that is used to show Posts
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Widget)
	TSubclassOf<UInviteeTextWidget> TextWidget;

	// Root Component
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	USceneComponent* RootSceneComponent;
	
	// Head Component
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Mesh, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* HeadMesh;
	
	// Face Component, used to visualise the Avatar on
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Mesh, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* FaceMesh;
	
	// Body Component
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Mesh, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* BodyMesh;
	
	// Emoji Widget Front
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Mesh, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* EmojiFrontMesh;
		
	// Text Widget Front
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Widget, meta = (AllowPrivateAccess = "true"))
	UWidgetComponent* TextFront;

	// Text Widget Back
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Widget, meta = (AllowPrivateAccess = "true"))
	UWidgetComponent* TextBack;
	
	// Spotlight Component, used during a Highlight event to highlight the player
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Light, meta = (AllowPrivateAccess = "true"))
	USpotLightComponent* SpotLight;

	UPROPERTY(BlueprintReadWrite, Category = Invitee)
	float EmojiTimer;

	// Invitee data
	UPROPERTY(BlueprintReadWrite, Category = Invitee, Meta = (ExposeOnSpawn = true))
	FReplyLiveInvitee InviteeData;
	
	// Flag to track if we are currently in a PostsHide event
	UPROPERTY(BlueprintReadWrite, Category = Invitee, Meta = (ExposeOnSpawn = true))
	bool isPostHidden;

	// Flag to track if we are currently in a PostCreated event
	UPROPERTY(BlueprintReadWrite, Category = Invitee, Meta = (ExposeOnSpawn = true))
	bool hasPost;

	// Flag to track if we are currently in a Highlight event
	UPROPERTY(BlueprintReadWrite, Category = Invitee, Meta = (ExposeOnSpawn = true))
	bool hasHighlight;

public:  // Public functions
	// Sets default values for this actor's properties
	AInvitee();

	/*
	Blueprint native events for custom implementation
	*/
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void SetFallbackAvatar();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void UpdateMe(FReplyLiveInvitee invitee);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void SetPost(const FString & Post);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void SetName(const FString & Name);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void HidePost();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void ShowPost();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void HighlightPost(const FString & Post);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void UnHighlightPost();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void BPSetAvatar(const FString & AvatarUrl);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void BPSetEmoji(const FString & Emoji);
	
	// Blueprint callable and not native, this is too complicated to implement in BP correctly
	UFUNCTION(BlueprintCallable)
	void SetEmoji(const FString & Emoji);

	/*
	Timed functions
	*/

	UFUNCTION(BlueprintCallable)
	void ShowEmoji(float ShowDuration);

	UFUNCTION(BlueprintCallable)
	void HideEmoji();

	UFUNCTION(BlueprintCallable)
	void ResetEmoji();

	// Delegate
	//UPROPERTY(BlueprintAssignable, Category = "EventDispatchers")
	//FOnDownloadFinish FinishedDownloadingAvatar;

private:  // Private variables
	FTimerHandle EmojiShowHandle;
	FTimerHandle EmojiHideHandle;

	bool ShowingEmoji;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called when constructed
	virtual void OnConstruction(const FTransform & Transform) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
