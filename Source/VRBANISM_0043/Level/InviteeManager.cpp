// Fill out your copyright notice in the Description page of Project Settings.


#include "InviteeManager.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
AInviteeManager::AInviteeManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
void AInviteeManager::BeginPlay()
{
	Super::BeginPlay();

	// We have to have an InviteeBlueprint selected. Otherwise we will get strange crashes due to nullpointers when executing code.
	// To avoid confusion we log an Error message and quit the application.
	if (!InviteeBlueprint) {
		UE_LOG(LogTemp, Error, TEXT("AInviteeManager::BeginPlay No Invitee selected, exiting application"));
		GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
	}

	// We have to have a DownloadManager selected. Otherwise we will get strange crashes due to nullpointers when executing code.
	// To avoid confusion we log an Error message and quit the application.
	//if (!DownloadManager) {
	//	UE_LOG(LogTemp, Error, TEXT("AInviteeManager::BeginPlay No DownloadManager selected, exiting application"));
	//	GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
	//}
}

// Called every frame
void AInviteeManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AInviteeManager::SpawnInvitee(FReplyLiveInvitee invitee)
{
	UE_LOG(LogTemp, Log, TEXT("AInviteeManager::SpawnInvitee Executing"));

	// Check if this is a new Invitee
	if (!Invitees.Contains(invitee.id)) {

		// Check if there are available seats
		bool hasSeats = SeatsManager->HasEmptySeat();
		if (hasSeats) {
			FTransform InviteeSpawnTransform = SeatsManager->GetEmptySeat();

			// DeferredSpawn so we can add parameters when spawning
			AInvitee* NewInvitee = GetWorld()->SpawnActorDeferred<AInvitee>(InviteeBlueprint, InviteeSpawnTransform);
			if (NewInvitee) {
				NewInvitee->isPostHidden = isPostsHidden;
				NewInvitee->InviteeData = invitee;
				NewInvitee->FinishSpawning(InviteeSpawnTransform);
			}
			else {
				UE_LOG(LogTemp, Error, TEXT("AInviteeManager::SpawnInvitee Unable to spawn invitee"));
			}

			AddInvitee(NewInvitee);

			//if (!invitee.avatar.IsEmpty()) {
				// We want to add this invitee to the download queue
			//	DownloadManager->AddDownload(NewInvitee);
			//}
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("AInviteeManager::SpawnInvitee No available seat"));
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("AInviteeManager::SpawnInvitee Attempted to spawn invitee that is already in Invitees, aborting"));
	}
}

void AInviteeManager::UpdateInvitee(FReplyLiveInvitee invitee)
{
	UE_LOG(LogTemp, Log, TEXT("AInviteeManager::UpdateInvitee Executing"));

	int32 InviteeId = invitee.id;
	bool boolHasSpawnedInvitee;
	AInvitee* SpawnedInvitee;
	
	hasSpawnedInvitee(InviteeId, boolHasSpawnedInvitee, SpawnedInvitee);
	if (boolHasSpawnedInvitee) {
		SpawnedInvitee->UpdateMe(invitee);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("UpdateInvitee We have not yet spawned Invitee with ID %d."), InviteeId);
	}
}

void AInviteeManager::AddInvitee(AInvitee* invitee)
{
	if (Invitees.Contains(invitee->InviteeData.id)) {
		UE_LOG(LogTemp, Warning, TEXT("AInviteeManager::AddInvitee Invitees already contains the invitee with id %d"), invitee->InviteeData.id);
	}
	else {
		Invitees.Add(invitee->InviteeData.id, invitee);
	}
}

void AInviteeManager::RemoveInvitee(AInvitee* invitee)
{
	if (Invitees.Contains(invitee->InviteeData.id)) {
		Invitees.Remove(invitee->InviteeData.id);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("AInviteeManager::RemoveInvitee Invitees does not contain the invitee"));
	}
}

void AInviteeManager::Posted(FReplyLivePostData PostData)
{
	UE_LOG(LogTemp, Log, TEXT("AInviteeManager::Posted Executing"));
	int32 InviteeId = PostData.invitee_id;
	FString Post = PostData.message;
	bool boolHasSpawnedInvitee;
	AInvitee* SpawnedInvitee;

	hasSpawnedInvitee(InviteeId, boolHasSpawnedInvitee, SpawnedInvitee);
	if (boolHasSpawnedInvitee) {
		// Show emoji so we know a post is being made.
		SpawnedInvitee->SetEmoji("post");

		SpawnedInvitee->SetPost(Post);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("AInviteeManager::Posted We have not yet spawned Invitee with ID %d."), InviteeId);
	}
}

void AInviteeManager::PostedEmoji(FReplyLivePostData PostEmojiData)
{
	UE_LOG(LogTemp, Log, TEXT("AInviteeManager::PostedEmoji Executing"));
	int32 InviteeId = PostEmojiData.invitee_id;
	FString Emoji = PostEmojiData.emoji;
	bool boolHasSpawnedInvitee;
	AInvitee* SpawnedInvitee;

	hasSpawnedInvitee(InviteeId, boolHasSpawnedInvitee, SpawnedInvitee);
	if (boolHasSpawnedInvitee) {
		SpawnedInvitee->SetEmoji(Emoji);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("AInviteeManager::PostedEmoji We have not yet spawned Invitee with ID %d."), InviteeId);
	}
}

void AInviteeManager::HideAllPosts()
{
	UE_LOG(LogTemp, Log, TEXT("AInviteeManager::HideAllPosts Executing"));
	TArray<AInvitee*> AllInvitees = GetInvitees();
	for (AInvitee* Invitee : AllInvitees) {
		Invitee->HidePost();
	}
	isPostsHidden = true;
}

void AInviteeManager::ShowAllPosts()
{
	UE_LOG(LogTemp, Log, TEXT("AInviteeManager::ShowAllPosts Executing"));
	TArray<AInvitee*> AllInvitees = GetInvitees();
	for (AInvitee* Invitee : AllInvitees) {
		Invitee->ShowPost();
	}
	isPostsHidden = false;
}

void AInviteeManager::HighlightPost(FReplyLivePostData PostData)
{
	// If we already have a highlight, we want to remove it before highlighting the new post.
	if (hasHighlightPost) {
		// Unhighlight the previous post first
		if (HighlightedInvitee) {
			HighlightedInvitee->UnHighlightPost();
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("AInviteeManager::HighlightPost We already have a highlighted post, but cant find its Invitee"));
		}
	}

	UE_LOG(LogTemp, Log, TEXT("AInviteeManager::HighlightPost Executing"));
	int32 InviteeId = PostData.invitee_id;
	FString Post= PostData.message;
	bool boolHasSpawnedInvitee;
	AInvitee* SpawnedInvitee;

	hasSpawnedInvitee(InviteeId, boolHasSpawnedInvitee, SpawnedInvitee);
	if (boolHasSpawnedInvitee) {
		SpawnedInvitee->HighlightPost(Post);
		HighlightedInvitee = SpawnedInvitee;
		hasHighlightPost = true;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("AInviteeManager::HighlightPost We have not yet spawned Invitee with ID %d."), InviteeId);
	}

	if (HighlightedInvitee->IsValidLowLevel()) {
		/*
		Move the Highlight Camera accordingly
		*/
		// Get the Invitee location
		FVector Location = HighlightedInvitee->GetActorLocation();

		// We use the RightVector since that is the vector that points forward from the Invitee...
		Location += HighlightedInvitee->GetActorRightVector() * HighlightCameraDistance;

		// Adjust the Camera height
		Location += FVector(0.0f, 0.0f, HighlightCameraHeight);

		// Set the Camera location
		HighlightCamera->SetActorLocation(Location);

		// Adjust the lookat height
		FRotator Rotation = UKismetMathLibrary::FindLookAtRotation(Location, (HighlightedInvitee->GetActorLocation() + FVector(0.0f, 0.0f, HighlightCameraFocuspointHeight)));

		// Set the Camera rotation
		HighlightCamera->SetActorRotation(Rotation);
	}
}

void AInviteeManager::UnHighlightPost(FReplyLivePostData PostData)
{
	UE_LOG(LogTemp, Log, TEXT("AInviteeManager::UnHighlightPost Executing"));
	int32 InviteeId = PostData.invitee_id;
	bool boolHasSpawnedInvitee;
	AInvitee* SpawnedInvitee;

	hasSpawnedInvitee(InviteeId, boolHasSpawnedInvitee, SpawnedInvitee);
	if (boolHasSpawnedInvitee) {
		SpawnedInvitee->UnHighlightPost();
		
		// Remove the highlight flags
		if (SpawnedInvitee == HighlightedInvitee) {
			HighlightedInvitee = nullptr;
			hasHighlightPost = false;
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("AInviteeManager::UnHighlightPost We have not yet spawned Invitee with ID %d."), InviteeId);
	}
}

void AInviteeManager::UnHighlightAllPosts()
{
	UE_LOG(LogTemp, Log, TEXT("AInviteeManager::UnHighlightAllPosts Executing"));
	TArray<AInvitee*> AllInvitees = GetInvitees();
	for (AInvitee* Invitee : AllInvitees) {
		Invitee->UnHighlightPost();
	}

	// Remove the highlight flags
	HighlightedInvitee = nullptr;
	hasHighlightPost = false;
}

void AInviteeManager::hasSpawnedInvitee(int32 InviteeId, bool& hasSpawnedInvitee, AInvitee*& Invitee)
{
	hasSpawnedInvitee = Invitees.Contains(InviteeId);
	if (hasSpawnedInvitee) {
		Invitee = Invitees[InviteeId];
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("AInviteeManager::hasSpawnedInvitee Invitee not yet spawned"));
		Invitee = NULL;
	}
}

TArray<AInvitee*> AInviteeManager::GetInvitees()
{
	TArray<AInvitee*> InviteesArray;
	Invitees.GenerateValueArray(InviteesArray);
	return InviteesArray;
}