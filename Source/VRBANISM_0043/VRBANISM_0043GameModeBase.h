// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "VRBANISM_0043GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class VRBANISM_0043_API AVRBANISM_0043GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
