/**
* Copyright (c) 2018 Cronofear Softworks, Inc. All Rights Reserved.
*
* Developed by Kevin Yabar Garces
*/

#pragma once
#include "Core/Public/PixelFormat.h"
#include "Async/Future.h"
#include "CSWImageIO.generated.h"

class UTexture2D;
class UObject;


DECLARE_DYNAMIC_DELEGATE_OneParam(FCSWOnImageLoadResponse, UTexture2D*, Texture2D); //Return a Texture2D after the image is loaded from disk
DECLARE_DYNAMIC_DELEGATE_OneParam(FCSWOnImageSaveResponse, const bool, bWasSuccesful); //Return a boolean after the image is saved on disk

/**
* Utility class for asynchronously loading an image into a texture.
* Allows Blueprint scripts to request asynchronous loading of an image and be notified when loading is complete.
*/
UCLASS(BlueprintType)
class CSWONLINESUBSYSTEM_API UCSWImageIO : public UObject
{
	GENERATED_BODY()
public:
#pragma region Static Functions
	/**
	* Save a Render Target to disk as a .png file.
	* @param RenderTarget				Render target with the texture information that will be saved to disk.
	* @param Path						Path where the image will be saved. (ex. using GetPath...()).
	* @param FileName					Name of the image. Extension is not needed (.png is used by default).
	* @return							Boolean result.
	*/
	UFUNCTION(BlueprintCallable, Category = "CSW|OnlineSubsystem::IO", meta = (DisplayName = "CSW::Save Image To Disk"))
		static bool CSWSaveRenderTargetToDisk(UTextureRenderTarget2D* RenderTarget, const FString& Path, const FString& FileName);
	//UFUNCTION(BlueprintCallable, Category = "CSW|OnlineSubsystem::IO", meta = (DisplayName = "CSW::Async Save Image To Disk", AutoCreateRefTerm = "OnResponse"))
		//static void AsyncSaveRenderTargetToDisk(UTextureRenderTarget2D* RenderTarget, const FString& Path, const FString& FileName, const FCSWOnImageSaveResponse& OnResponse);//Doesn't work if it's not running on the GameThread.

	/**
	* Loads an image file from disk into a texture2d. Can load various types of images.
	* @param Outer						Object responsible of creating the texture loaded.
	* @param Path						Path from where the image will be loaded. (ex. using GetPath...()).
	* @param FileName					Name of the image file.
	* @param FileExtension				Extension of the image file.
	* @return							A texture2D created from the loaded image file.
	*/
	UFUNCTION(BlueprintCallable, Category = "CSW|OnlineSubsystem::IO", meta = (DisplayName = "CSW::Load Image From Disk", FileExtension = ".png", HidePin = "Outer", DefaultToSelf = "Outer"))
		static void CSWLoadImageFromDisk(UObject* Outer, const FString& Path, const FString& FileName, const FString& FileExtension, UTexture2D*& Texture2D);
	UFUNCTION(BlueprintCallable, Category = "CSW|OnlineSubsystem::IO", meta = (DisplayName = "CSW::Async Load Image From Disk", FileExtension = ".png", AutoCreateRefTerm = "OnResponse", HidePin = "Outer", DefaultToSelf = "Outer"))
		static void AsyncLoadImageFromDisk(UObject* Outer, const FString& Path, const FString& FileName, const FString& FileExtension, const FCSWOnImageLoadResponse& OnResponse);
#pragma endregion
private:

#pragma region Internal
	/**
	* Create a CSWImageIO handler.
	*/
	UFUNCTION()
		static UCSWImageIO* CreateCSWImageIO();
	/**
	* Save a render target to disk as a png file.
	* @return			true if the render target was saved
	*/
	UFUNCTION()
		static bool SaveRenderTargetToDisk(UTextureRenderTarget2D* InRenderTarget, const FString& ImagePath, const FString& ImageName, bool bIsAsync = false);
	/**
	* Loads an image file from disk into a texture. This will block the calling thread until completed.
	* @return A texture created from the loaded image file.
	*/
	UFUNCTION()
		static UTexture2D* LoadImageFromDisk(UObject* Outer, const FString& ImagePath, bool bIsAsync = false);

	/**
	* Create a Texture2D from PixelData array.
	*/
	UFUNCTION()
		static UTexture2D* CreateTexture(UObject* Outer, const TArray<uint8>& PixelData, int32 InSizeX, int32 InSizeY, EPixelFormat InFormat = EPixelFormat::PF_B8G8R8A8, FName BaseName = NAME_None);

#pragma endregion

#pragma region Async Internal

	/** Helper function that initiates the loading operation and fires the event when loading is done. */
	void SaveImageAsync(UTextureRenderTarget2D* InRenderTarget, const FString& ImagePath, const FString& ImageName);
	/**
	Loads an image file from disk into a texture on a worker thread. This will not block the calling thread.
	@return A future object which will hold the image texture once loading is done.
	*/
	static TFuture<bool> SaveRenderTargetToDiskAsync(UTextureRenderTarget2D* InRenderTarget, const FString& ImagePath, const FString& ImageName, TFunction<void()> CompletionCallback);


	/** Helper function that initiates the loading operation and fires the event when loading is done. */
	void LoadImageAsync(UObject* Outer, const FString& ImagePath);
	/**
	Loads an image file from disk into a texture on a worker thread. This will not block the calling thread.
	@return A future object which will hold the image texture once loading is done.
	*/
	static TFuture<UTexture2D*> LoadImageFromDiskAsync(UObject* Outer, const FString& ImagePath, TFunction<void()> CompletionCallback);

#pragma endregion
private:
	/**
	* Callback on response.
	*/
	FCSWOnImageLoadResponse InternalOnImageLoadResponse;
	FCSWOnImageSaveResponse InternalOnImageSaveResponse;

	/** Holds the future value which represents the asynchronous loading operation. */
	TFuture<UTexture2D*> LoadFuture;
	/** Holds the future value which represents the asynchronous loading operation. */
	TFuture<bool> SaveFuture;
};
