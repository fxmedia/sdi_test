/**
* Copyright (c) 2018 Cronofear Softworks, Inc. All Rights Reserved.
*
* Developed by Kevin Yabar Garces
*/

#include "Async/CSWFilewwwIO.h"
#include "HAL/PlatformFilemanager.h"
#include "Misc/Base64.h"
#include "Serialization/BufferArchive.h"
#include "Misc/Paths.h"
#include "Misc/FileHelper.h"

UCSWFilewwwIO::UCSWFilewwwIO()
{
}

UCSWFilewwwIO::~UCSWFilewwwIO()
{
}

void UCSWFilewwwIO::AsyncDownloadFile(const FString& URL, const FString& Path, const FString& FileName, const FString& FileExtension, const TArray<FString>& Params, const FCSWOnFilewwwIOResponse& OnResponse, const FCSWOnFilewwwIOProgress& OnProgress)
{
	/// Validation
	if (!FPaths::DirectoryExists(Path))
	{
		UE_LOG(LogTemp, Warning, TEXT("AsyncDownloadFile: Directory doesn't exists"));
		return;
	}
	if (URL.Len() <= 0 || FileName.Len() <= 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("AsyncDownloadFile: URL and FileName should contain characters"));
		return;
	}
	/// Start Download File
	UCSWFilewwwIO* FileWWWIO = UCSWFilewwwIO::CreateCSWFilewwwIO();
	FileWWWIO->OnResponseData.URL = URL;
	FileWWWIO->OnResponseData.Path = Path;
	FileWWWIO->OnResponseData.FileName = FileName;
	FileWWWIO->OnResponseData.FileExtension = FileExtension;
	FileWWWIO->OnResponseData.Params = Params;
	FileWWWIO->InternalOnResponse = OnResponse;
	FileWWWIO->InternalOnProgress = OnProgress;
	FileWWWIO->DownloadFile();
}

void UCSWFilewwwIO::AsyncUploadFile(const FString& URL, const FString& Path, const FString& FileName, const FString& FileExtension, const FString& ContentType, const TArray<FString>& Params, const FCSWOnFilewwwIOResponse& OnResponse, const FCSWOnFilewwwIOProgress& OnProgress)
{
	/// Validation
	if (!FPaths::DirectoryExists(Path))
	{
		UE_LOG(LogTemp, Warning, TEXT("AsyncUploadFile: Directory doesn't exists"));
		return;
	}
	if (URL.Len() <= 0 || FileName.Len() <= 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("AsyncUploadFile: URL and FileName should contain characters"));
		return;
	}
	/// Start Upload File
	UCSWFilewwwIO* FileWWWIO = UCSWFilewwwIO::CreateCSWFilewwwIO();
	FileWWWIO->OnResponseData.URL = URL;
	FileWWWIO->OnResponseData.Path = Path;
	FileWWWIO->OnResponseData.FileName = FileName;
	FileWWWIO->OnResponseData.FileExtension = FileExtension;
	FileWWWIO->OnResponseData.Params = Params;
	FileWWWIO->InContentType = ContentType;
	FileWWWIO->InternalOnResponse = OnResponse;
	FileWWWIO->InternalOnProgress = OnProgress;
	FileWWWIO->UploadFile();
}

UCSWFilewwwIO* UCSWFilewwwIO::CreateCSWFilewwwIO()
{
	UCSWFilewwwIO* FileWWWIO = NewObject<UCSWFilewwwIO>();
	return FileWWWIO;
}

UCSWFilewwwIO* UCSWFilewwwIO::DownloadFile()
{
	InFullPath = OnResponseData.Path + OnResponseData.FileName + OnResponseData.FileExtension;
	/// Create GET Header
	auto HttpRequest = FHttpModule::Get().CreateRequest();
	HttpRequest->SetVerb("GET");
	HttpRequest->SetURL(*OnResponseData.URL);
	/// Bind Response and Progress events
	HttpRequest->OnProcessRequestComplete().BindUObject(this, &UCSWFilewwwIO::OnDownloadReady);
	HttpRequest->OnRequestProgress().BindUObject(this, &UCSWFilewwwIO::OnDownloadProgress_Internal);
	/// Process the request
	HttpRequest->ProcessRequest();
	AddToRoot();
	/// Return this object
	return this;
}

UCSWFilewwwIO* UCSWFilewwwIO::UploadFile()
{
	InFullPath = OnResponseData.Path + OnResponseData.FileName + OnResponseData.FileExtension;
	/// Create POST Header
	auto HttpRequest = FHttpModule::Get().CreateRequest();
	HttpRequest->SetURL(*OnResponseData.URL);
	HttpRequest->SetVerb(TEXT("POST"));
	HttpRequest->SetHeader(TEXT("Content-Type"), TEXT("multipart/form-data; charset=utf-8; boundary=cswboundarycsw"));
	FString a = TEXT("\r\n--cswboundarycsw\r\n");
	FString b = FString::Printf(TEXT("Content-Disposition: form-data; name=\"file\";  filename=\"%s\"\r\n"), *OnResponseData.FileName);
	FString c = FString::Printf(TEXT("Content-Type: %s\r\n\r\n"), *InContentType);
	TArray<uint8> d; FFileHelper::LoadFileToArray(OUT d, *InFullPath);
	FString e = TEXT("\r\n--cswboundarycsw--\r\n");
	///Convert Header String to Array of Bytes
	TArray<uint8> FinalData;
	FinalData.Append((uint8*)TCHAR_TO_UTF8(*a), a.Len()); ///This process could be faster, investigate it how.
	FinalData.Append((uint8*)TCHAR_TO_UTF8(*b), b.Len());
	FinalData.Append((uint8*)TCHAR_TO_UTF8(*c), c.Len());
	FinalData.Append(d);
	FinalData.Append((uint8*)TCHAR_TO_UTF8(*e), e.Len());
	///Set Binary data
	HttpRequest->SetContent(FinalData);
	///Bind Events
	HttpRequest->OnProcessRequestComplete().BindUObject(this, &UCSWFilewwwIO::OnUploadReady);
	HttpRequest->OnRequestProgress().BindUObject(this, &UCSWFilewwwIO::OnUploadProgress_Internal);
	///Execute the request and add it to root (prevent garbage collection)
	HttpRequest->ProcessRequest();
	AddToRoot();
	return this;
}

void UCSWFilewwwIO::OnDownloadReady(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	/// Remove process from root
	RemoveFromRoot();
	Request->OnProcessRequestComplete().Unbind();
	/// Save downloaded file to disk
	if (Response.IsValid() && EHttpResponseCodes::IsOk(Response->GetResponseCode()))
	{
		/// SAVE FILE
		IPlatformFile& PlatformFile = FPlatformFileManager::Get().GetPlatformFile();
		/// open/create the file
		IFileHandle* FileHandle = PlatformFile.OpenWrite(*InFullPath);
		if (FileHandle)
		{
			/// write the file
			FileHandle->Write(Response->GetContent().GetData(), Response->GetContentLength());
			/// Close the file
			delete FileHandle;
			/// RESPONSE CALLBACK SUCCESS
			InternalOnResponse.ExecuteIfBound(true, OnResponseData);
			return;
		}
	}
	/// RESPONSE CALLBACK FAILED
	InternalOnResponse.ExecuteIfBound(false, OnResponseData);
}

void UCSWFilewwwIO::OnDownloadProgress_Internal(FHttpRequestPtr Request, int32 BytesSent, int32 BytesReceived)
{
	int32 FullSize = Request->GetContentLength();
	InternalOnProgress.ExecuteIfBound(BytesSent, BytesReceived, FullSize);
}

void UCSWFilewwwIO::OnUploadReady(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	/// Remove process from root
	RemoveFromRoot();
	Request->OnProcessRequestComplete().Unbind();
	/// Upload File Response
	if (Response.IsValid() && Response->GetResponseCode() == 200) ///EHttpResponseCodes::IsOk(Response->GetResponseCode())
	{
		/// RESPONSE CALLBACK SUCCESS
		InternalOnResponse.ExecuteIfBound(true, OnResponseData);
		return;
	}
	/// RESPONSE CALLBACK FAILED
	InternalOnResponse.ExecuteIfBound(false, OnResponseData);
}

void UCSWFilewwwIO::OnUploadProgress_Internal(FHttpRequestPtr Request, int32 BytesSent, int32 BytesReceived)
{
	int32 FullSize = Request->GetContentLength();
	InternalOnProgress.ExecuteIfBound(BytesSent, BytesReceived, FullSize);
}
