/**
* Copyright (c) 2018 Cronofear Softworks, Inc. All Rights Reserved.
*
* Developed by Kevin Yabar Garces
*/

#include "BlueprintFunctionLibrary/CSWOnlineSubsystemLibrary.h"
#include "HAL/PlatformFilemanager.h"
#include "Kismet/KismetStringLibrary.h"
#include "Misc/Paths.h"
#include "GenericPlatform/GenericPlatformFile.h"
#include "HAL/FileManager.h"

FString UCSWOnlineSubsystemLibrary::GetPathProject()
{
	return FPaths::ConvertRelativePathToFull(FPaths::ProjectDir());
}

FString UCSWOnlineSubsystemLibrary::GetPathSaved()
{
	return FPaths::ConvertRelativePathToFull(FPaths::ProjectSavedDir());
}

FString UCSWOnlineSubsystemLibrary::GetPathScreenshots()
{
	return FPaths::ConvertRelativePathToFull(FPaths::ProjectSavedDir() + TEXT("Screenshots/"));
}

FString UCSWOnlineSubsystemLibrary::GetPathSaveGames()
{
	return FPaths::ConvertRelativePathToFull(FPaths::ProjectSavedDir() + TEXT("SaveGames/"));
}

FString UCSWOnlineSubsystemLibrary::GetPlatformName()
{
	return FPlatformProperties::IniPlatformName();
}

bool UCSWOnlineSubsystemLibrary::DoesDirectoryExists(const FString& Path)
{
	if (Path.Len() <= 0) return false;

	return FPaths::DirectoryExists(*Path);
}

bool UCSWOnlineSubsystemLibrary::DoesFileExists(const FString& Path, const FString& FileName, const FString& Extension)
{
	if (Path.Len() <= 0) return false;

	FString FullPath = Path + FileName + Extension;
	return FPaths::FileExists(*FullPath);
}

void UCSWOnlineSubsystemLibrary::GetMetaDataFrom(const FString& Path, const FString& FileName, const FString& Extension, FCSWDirectoryData& Metadata)
{
	if (Path.Len() <= 0) return;
	///Get the metadata of the file or directory in c++
	FString FullPath = Path + FileName + Extension;
	IPlatformFile& PlatformFileManager = FPlatformFileManager::Get().GetPlatformFile();
	FFileStatData FileStatData = PlatformFileManager.GetStatData(*FullPath);
	///Copy the data in a structure that Blueprints can read
	Metadata.AccessTime = FileStatData.AccessTime;
	Metadata.bIsDirectory = FileStatData.bIsDirectory;
	Metadata.bIsReadOnly = FileStatData.bIsReadOnly;
	Metadata.bIsValid = FileStatData.bIsValid;
	Metadata.CreationTime = FileStatData.CreationTime;
	Metadata.ModificationTime = FileStatData.ModificationTime;
}

bool UCSWOnlineSubsystemLibrary::MakeDirectory(const FString& Path)
{
	if (Path.Len() <= 0) return false;

	IPlatformFile& PlatformFileManager = FPlatformFileManager::Get().GetPlatformFile();
	if (!FPaths::DirectoryExists(*Path))
	{
		return PlatformFileManager.CreateDirectory(*FPaths::GetPath(Path));
	}
	return false;
}

void UCSWOnlineSubsystemLibrary::GetFilesInDirectory(TArray<FString>& FileNames, const FString& Path, const FString& FileExtension, bool bRemoveExtensions)
{
	/// Validation
	if (Path.Len() <= 0 || FileExtension.Len() <= 0) return;
	/// Find files in folder with extension
	IFileManager& FileManager = IFileManager::Get();
	FileManager.FindFiles(OUT FileNames, *Path, *FileExtension);
	/// remove extensions of the names
	if (bRemoveExtensions)
	{
		for (FString& FileName : FileNames)
		{
			int32 Len = UKismetStringLibrary::FindSubstring(FileName, TEXT("."), false, true);
			FileName = UKismetStringLibrary::GetSubstring(FileName, 0, Len);
		}
	}
}

bool UCSWOnlineSubsystemLibrary::DeleteFile(const FString& Path, const FString& FileName, const FString& FileExtension)
{
	if (Path.Len() <= 0) return false;

	const FString FullPath = Path + FileName + FileExtension;
	return FPlatformFileManager::Get().GetPlatformFile().DeleteFile(*FullPath);
}

bool UCSWOnlineSubsystemLibrary::MoveFile(const FString& SourcePath, const FString& FileName, const FString& FileExtension, const FString& DestinationPath)
{
	if (SourcePath.Len() <= 0 || DestinationPath.Len() <= 0) return false;

	const FString SourceFullPath = SourcePath + FileName + FileExtension;
	const FString DestinationFullPath = DestinationPath + FileName + FileExtension;
	return FPlatformFileManager::Get().GetPlatformFile().MoveFile(*DestinationFullPath, *SourceFullPath);
}

bool UCSWOnlineSubsystemLibrary::CopyFile(const FString& SourcePath, const FString& FileName, const FString& FileExtension, const FString& DestinationPath)
{
	if (SourcePath.Len() <= 0 || DestinationPath.Len() <= 0) return false;

	const FString SourceFullPath = SourcePath + FileName + FileExtension;
	const FString DestinationFullPath = DestinationPath + FileName + FileExtension;
	return FPlatformFileManager::Get().GetPlatformFile().CopyFile(*DestinationFullPath, *SourceFullPath);
}
